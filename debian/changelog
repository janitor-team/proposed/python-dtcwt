python-dtcwt (0.12.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild to drop ancient sphinx Built-Using version.
  * debian/: Apply "wrap-and-sort -abst".
  * debian/control: Drop legacy build-dep dpkg-dev.
  * debian/control: Bump debhelper-compat to v13.
  * debian/control: Bump Standards-Version to 4.6.1.

 -- Boyuan Yang <byang@debian.org>  Fri, 10 Jun 2022 20:47:40 -0400

python-dtcwt (0.12.0-2) unstable; urgency=medium

  * Team upload.
  * Use Python 3 for building docs.
  * Drop Python 2 support (Closes: #937718).
  * d/control: Remove ancient X-Python-Version field.
  * d/control: Remove ancient X-Python3-Version field.
  * d/copyright: Use https protocol in Format field.
  * Use debhelper-compat instead of debian/compat.
  * d/control: Set Vcs-* to salsa.debian.org.

 -- Ondřej Nový <onovy@debian.org>  Thu, 24 Oct 2019 14:16:07 +0200

python-dtcwt (0.12.0-1) unstable; urgency=medium

  * Sign tags on gbp import
  * Upgrade watch file to version 4
  * New upstream version 0.12.0
  * Drop superfluous Testsuite field
  * Bump debhelper version to 10
  * Move pyopencl dependency to Suggests
  * Fixup the upstream name in copyright
  * Update the copyright dates
  * Update the BSD-2-Clause paragraph
  * Update the binary package descriptions
  * Fixup the proxy settings for the docs
  * Add new dependency on the Sphinx RTD theme
  * Build the docs with Python 3
  * Add support for the nodoc build profile
  * Add the Build-Using metadata for the docs
  * Add recommended get-orig-source target
  * Simplify integration with the system MathJax
    - Hardcode the path to MathJax in conf.py
    - Drop the .links file, no longer required
    - Drop dh_sphinxdoc override, no longer required
  * Add new test dependency on scipy
  * Add support for the nocheck build profile
  * Run autopkgtests for all supported Python versions
  * Bump standards version to 4.1.0

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Fri, 15 Sep 2017 11:36:42 +0100

python-dtcwt (0.11.0-2) unstable; urgency=medium

  * Make build reproducible.
  * Simplify packaging testsuite using Test-Command.
  * cme fix dpkg-control:
    - Drop unnecessary versioned dependency on sphinx.
    - Bump standards version to 3.9.8, no changes required.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 03 Aug 2016 13:01:59 +0100

python-dtcwt (0.11.0-1) unstable; urgency=medium

  * New upstream release.
  * DFSG cleaning is no longer required:
    - d/copyright: remove Files-Excluded entry.
    - d/gbp.conf: synchronise with upstream master branch and tags.
    - d/watch: remove repacksuffix option.
  * Update patch queue:
    - Refresh use-system-mathjax.patch and reproducible-build.patch.
    - Drop Fix-usage-of-unsafe-inplace-casting.patch, applied upstream.
    - Drop Use-explicit-integer-division.patch, applied upstream.
  * d/{control,tests/*}: replace dependency on nose with pytest.
  * d/gbp.conf: switch to DEP-14 repository layout.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 10 Mar 2016 14:02:22 +0000

python-dtcwt (0.10.1+dfsg1-4) unstable; urgency=medium

  * Clean documentation build and egg-info directories.
  * Add patch fixing runtime failure with Python 3.
    File: Use-explicit-integer-division.patch
  * Bump standards version to 3.9.7, no changes required.
  * d/watch: add missing repacksuffix option.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Tue, 08 Mar 2016 10:26:31 +0000

python-dtcwt (0.10.1+dfsg1-3) unstable; urgency=medium

  * d/control: use secure VCS-Git URI.
  * d/rules: simplify doc install targets.
  * d/rules: remove test override, use pybuild instead.
  * Add patch fixing an FTBFS error in the testsuite.
    File: Fix-usage-of-unsafe-inplace-casting.patch
  * Add autopkgtest testsuite.
  * d/control: move dependency on pyopencl to Recommends.
  * d/control: cme fixed.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Wed, 27 Jan 2016 17:31:04 +0000

python-dtcwt (0.10.1+dfsg1-2) unstable; urgency=medium

  * Make build reproducible. (Closes: #794005)
    Thanks to Chris Lamb for providing the patch.

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Sat, 08 Aug 2015 20:25:15 +0100

python-dtcwt (0.10.1+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #793139)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 20 Jul 2015 23:05:14 +0100
